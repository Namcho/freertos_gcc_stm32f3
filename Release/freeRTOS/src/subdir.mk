################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../freeRTOS/src/croutine.c \
../freeRTOS/src/event_groups.c \
../freeRTOS/src/heap_4.c \
../freeRTOS/src/list.c \
../freeRTOS/src/queue.c \
../freeRTOS/src/tasks.c \
../freeRTOS/src/timers.c 

OBJS += \
./freeRTOS/src/croutine.o \
./freeRTOS/src/event_groups.o \
./freeRTOS/src/heap_4.o \
./freeRTOS/src/list.o \
./freeRTOS/src/queue.o \
./freeRTOS/src/tasks.o \
./freeRTOS/src/timers.o 

C_DEPS += \
./freeRTOS/src/croutine.d \
./freeRTOS/src/event_groups.d \
./freeRTOS/src/heap_4.d \
./freeRTOS/src/list.d \
./freeRTOS/src/queue.d \
./freeRTOS/src/tasks.d \
./freeRTOS/src/timers.d 


# Each subdirectory must supply rules for building sources it contributes
freeRTOS/src/%.o: ../freeRTOS/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra  -g -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F30X -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../freeRTOS/header" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


