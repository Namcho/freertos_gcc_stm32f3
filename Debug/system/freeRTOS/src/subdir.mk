################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/freeRTOS/src/croutine.c \
../system/freeRTOS/src/event_groups.c \
../system/freeRTOS/src/heap_4.c \
../system/freeRTOS/src/list.c \
../system/freeRTOS/src/queue.c \
../system/freeRTOS/src/tasks.c \
../system/freeRTOS/src/timers.c 

OBJS += \
./system/freeRTOS/src/croutine.o \
./system/freeRTOS/src/event_groups.o \
./system/freeRTOS/src/heap_4.o \
./system/freeRTOS/src/list.o \
./system/freeRTOS/src/queue.o \
./system/freeRTOS/src/tasks.o \
./system/freeRTOS/src/timers.o 

C_DEPS += \
./system/freeRTOS/src/croutine.d \
./system/freeRTOS/src/event_groups.d \
./system/freeRTOS/src/heap_4.d \
./system/freeRTOS/src/list.d \
./system/freeRTOS/src/queue.d \
./system/freeRTOS/src/tasks.d \
./system/freeRTOS/src/timers.d 


# Each subdirectory must supply rules for building sources it contributes
system/freeRTOS/src/%.o: ../system/freeRTOS/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F30X -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


