/*
 * LedClass.cpp
 *
 *  Created on: 26 May 2015
 *      Author: Namcho
 */

#include "LedClass.h"
#include "stm32f30x.h"
#include "stm32f30x_rcc.h"
#include "stm32f30x_gpio.h"


LedClass::LedClass() {
	// TODO Auto-generated constructor stub
	initPort();
}

LedClass::~LedClass() {
	// TODO Auto-generated destructor stub
	//Port deinit yapilcak
}

void LedClass::setPort(uint16_t val){
	GPIO_Write(GPIOD, val);
}

uint16_t LedClass::getPort(void){

	return GPIO_ReadInputData(GPIOD);
}

void LedClass::setPin(uint16_t LEDx, bool pinVal){
	if(pinVal == true)
		GPIO_WriteBit(GPIOD,LEDx, Bit_SET);
	else
		GPIO_WriteBit(GPIOD,LEDx, Bit_RESET);
}

bool LedClass::getPin(void){

	return false;
}

void LedClass::initPort(void){
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD,ENABLE);
	GPIO_InitTypeDef gpInit;
	gpInit.GPIO_Mode = GPIO_Mode_OUT;
	gpInit.GPIO_OType = GPIO_OType_PP;
	gpInit.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpInit.GPIO_Speed = GPIO_Speed_2MHz;
	gpInit.GPIO_Pin =  LED1 | LED2 | LED3 | LED4;
	GPIO_Init(GPIOD, &gpInit);
}

void LedClass::run(void){
	uint8_t led=0;

	for(;;){
		led = led ^ 0x01;

		if(led){
			setPin(LED1,true);
			setPin(LED2,true);
			setPin(LED3,true);
			setPin(LED4,true);
		}
		else{
			setPin(LED1,false);
			setPin(LED2,false);
			setPin(LED3,false);
			setPin(LED4,false);
		}

		TaskOOP::delay(500);
	}
}

