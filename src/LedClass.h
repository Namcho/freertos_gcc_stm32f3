/*
 * LedClass.h
 *
 *  Created on: 26 May 2015
 *      Author: Namcho
 */

#ifndef LEDCLASS_H_
#define LEDCLASS_H_


#define _LED1	GPIO_Pin_4
#define _LED2	GPIO_Pin_5
#define _LED3	GPIO_Pin_6
#define _LED4	GPIO_Pin_7

//STM32F3Disco  Led definations
#define LED1_D	GPIO_Pin_9
#define LED2_D	GPIO_Pin_10
#define LED3_D	GPIO_Pin_11
#define LED4_D	GPIO_Pin_12

#define LED1	_LED1
#define LED2	_LED2
#define LED3	_LED3
#define LED4	_LED4

//#define GPIOxCLOCK		RCC_AHBPeriph_GPIOE
//#define GPIOx			GPIOE


#include "TaskOOP.h"
#include "IGpio.h"

class LedClass : public TaskOOP , public IGpio{
public:
	LedClass();
	virtual ~LedClass();
	virtual void setPort(uint16_t portVal);
	virtual uint16_t getPort(void);
	virtual void setPin(uint16_t LEDx, bool pinVal);
	virtual bool getPin(void);
private:
	virtual void initPort(void);

protected:
	virtual void run();
};

#endif /* LEDCLASS_H_ */
