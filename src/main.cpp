//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"
#include "LedClass.h"

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"


static LedClass ledObj;

int
main(int argc, char* argv[])
{
	//Systick init'i kur.
	SystemInit();
	//SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
	if (SysTick_Config(SystemCoreClock / 1000))
	{
	  /* Capture error */
	  while (1);
	}


	ledObj.create("LedTask",configMINIMAL_STACK_SIZE,1);
	TaskOOP::taskStartScheduler();
	//vTaskStartScheduler();


	while (1)
	{

	}
}


#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
